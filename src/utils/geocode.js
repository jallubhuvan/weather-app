const request = require('request');

const geoCode = (address, callback) => {
    
    const mapBoxUrl = `https://api.mapbox.com/geocoding/v5/mapbox.places/${encodeURIComponent(address)}.json?access_token=pk.eyJ1IjoidXNyZW1hbmUiLCJhIjoiY2tqdGhiMGo3MmVwZjJ4dGZtZ3cybms0biJ9.BCRyMg3GWXZjYniVUosIuw&limit=1`

    request({url: mapBoxUrl, json: true}, (error, {body} = {}) => {
        if(error) {
            callback('Unable to connect to location service.', undefined);
        } else if (body.features.length === 0) {
            callback('Unable to find location. PLease try another search', undefined);
        } else {
           const latLong = body.features[0].center;
           callback(undefined,{
               latitude: latLong[1],
               longitude: latLong[0],
               location: body.features[0].place_name
            });

        }

    }) 
}

module.exports = {geoCode}