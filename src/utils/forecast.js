const request = require('request');

const forecast = (latitude,longitude, location,  callback) => {
    const weatherUrl = `http://api.weatherstack.com/current?access_key=834a31ffd02640b2afb386f7cdc801fd&query=${latitude},${longitude}`;

    request({url: weatherUrl, json: true}, (error, {body} = {}) => {
        if (error) {
            callback('Unable to connect to weather service.', undefined);
        } else if (body.error) {
            callback('Unable to find location.', undefined);
    
        } else {
            const foreCast = `For [${location}] it is currently ${body.current.temperature} degrees out. But it feels like ${body.current.feelslike} degrees with ${body.current.humidity} humidity.`
            callback(undefined, {foreCast});
        }
    })
    
}

module.exports = {forecast}