const express = require('express');
const path = require('path');
const hbs = require('hbs');
const {forecast} = require('./utils/forecast');
const {geoCode} = require('./utils/geocode');

const app = express();
const port = process.env.PORT || 3000;

const publicPath = path.join(__dirname, '../public');
const viewsPath= path.join(__dirname,'../templates/views');
const partialsPath= path.join(__dirname,'../templates/partials');


//set up for view engine and location
app.set('view engine', 'hbs');
app.set('views', viewsPath);
hbs.registerPartials(partialsPath);

//set up for static directory
app.use(express.static(publicPath));

app.get('', (req, res) => {
    res.render('index', {
        title: 'Weather app',
        name: 'Bhuvan'
    });
})

app.get('/about', (req, res) => {
    res.render('about', {
        title: 'About Me',
        name: 'Bhuvan'
    });
})

app.get('/help', (req, res) => {
    res.render('help', {
        title: 'Help',
        name: 'Bhuvan',
        message: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Asperiores, odio'
    })
})

app.get('/weather', (req, res) => {

    const {address} = req.query;
    if (!address) {
        return res.send({error: 'Please provide the adderess'});
    } 
    geoCode(address, (geocodeError, {latitude, longitude, location} = {}) => {
        if(geocodeError) {
            return res.send({error: geocodeError});
        } else {

            forecast(latitude, longitude, location,(forecastError, {foreCast} = {}) => {
                if (forecastError) {
                    return res.send({error: forecastError});
                } else {
                    return res.send({foreCast, location});
                }
            });

        }
    })
});

app.get('/products', (req, res) => {

    if (!req.query.search) {
        console.log('Query param not found.');
        return res.send({error: 'Please provide search value'});
    }
    res.send({products:[]});
});

app.get('/help/*', (req,res) => {
    res.render('pageNotFound', {
        title: 'Page Not Found!!!',
        name: 'Bhuvan',
        template: 'Help article not found'
    });
})

app.get('*', (req,res) => {
    res.render('pageNotFound', {
        title: 'Page Not Found!!!',
        name: 'Bhuvan',
        template: 'The page you are trying to reach  not exists.'
    });
})


app.listen(port, () => {
    console.log(`Server is up on port ${port}`);
});