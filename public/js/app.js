console.log('Clinet side loaded')

const weatherForm = document.querySelector('form');
const searchEle = document.querySelector('input');
const buttonEle = document.querySelector('button');
const messageOne = document.querySelector('#messageOne');
const messageTwo = document.querySelector('#messageTwo');

weatherForm.addEventListener('submit', (event) => {
    messageOne.textContent = 'Loading!!!!!!';
    messageTwo.textContent = '';
    buttonEle.disabled = true;
    event.preventDefault();
    
    const location = searchEle.value;
    if (location && location.trim()) {
        //http://localhost:3000   (local env)
        const url =`/weather?address=${location}`
        fetch(url).then((res) => {
            res.json().then((data) => {
                    if (data.error) {
                        messageTwo.textContent = data.error
                    } else {
                        messageOne.textContent = data.location;
                        messageTwo.textContent = data.foreCast;

                    }
                    buttonEle.disabled = false, 600
                });
});


    }
})